package sivps.aits.animalidreader;

import android.bluetooth.BluetoothDevice;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import rid.ptdevice.BTManager;
import rid.ptdevice.BTManager.TagListener;
import rid.ptdevice.BTManager.TagMonitor;
import rid.ptdevice.Tag;


public class AitsAnimalIdReaderModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public AitsAnimalIdReaderModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "AitsAnimalIdReader";
  }

  @ReactMethod
  public void Scan(Promise promise) {
    Set<BluetoothDevice> pairedDevices = BTManager.getBondedPTDevices();
    List<BluetoothDevice> list = new ArrayList(pairedDevices);
    WritableArray pairedDeviceAddresses = new WritableNativeArray();
    for (BluetoothDevice device : pairedDevices) {
	WritableMap d = new WritableNativeMap();
	d.putString("name", device.getName());
	d.putString("address", device.getAddress());
        pairedDeviceAddresses.pushMap(d);
    }
    promise.resolve(pairedDeviceAddresses);
  }

  @ReactMethod
  public void Read(final String address, final Promise promise) {
    try {
      Set<BluetoothDevice> pairedDevices = BTManager.getBondedPTDevices();
      List<BluetoothDevice> list = new ArrayList(pairedDevices);
      TagMonitor createMonitor = BTManager.startMonitor(address);

      TagListener tagListener = new TagListener() {
        @Override
        public void onReceive(String strType, String strContent) {
          final String tagType = strType.trim().toUpperCase();
          if ("FDXB".equals(tagType)) {
            Tag tag = TagMonitor.parseRecord(strType, strContent);
            promise.resolve(tag.getId());
          } else {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < strContent.length(); i = i + 2) {
              String s = strContent.substring(i, i + 2);
              int n = Integer.valueOf(s, 16);
              builder.append((char)n);
            }
            promise.resolve(builder.toString());
          }
          BTManager.DeviceHandler deviceHandler = BTManager.getDeviceHandler(address);
          deviceHandler.close();
        }
      };
      createMonitor.bindTagListener(tagListener);
    } catch (Exception e) {

    }
  }
}
