
# react-native-animal-id-reader

## Getting started

`$ npm install react-native-animal-id-reader --save`

### Mostly automatic installation

`$ react-native link react-native-animal-id-reader`

### Manual installation


#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import sivps.aits.animalidreader.AitsAnimalIdReaderPackage;` to the imports at the top of the file
  - Add `new AitsAnimalIdReaderPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-animal-id-reader'
  	project(':react-native-animal-id-reader').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-animal-id-reader/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-animal-id-reader')
  	```


## Usage
```javascript
import AitsAnimalIdReader from 'react-native-animal-id-reader';

// TODO: What to do with the module?
AitsAnimalIdReader;
```
  